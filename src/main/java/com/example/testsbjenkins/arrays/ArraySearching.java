package com.example.testsbjenkins.arrays;

import java.util.Arrays;

/**
 * @author: lt
 * @date: 2022/6/23 16:19
 * @Description: todo
 */
public class ArraySearching {
    public static void main(String[] args) {
    
        int[] array ={99,1,2,3,5,15};
        Arrays.sort(array);
        System.out.println("sort 后的:"+Arrays.toString(array));

        System.out.println("1 所在的下标:"+Arrays.binarySearch(array,1));
    }
}
