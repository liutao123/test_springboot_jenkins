package com.example.testsbjenkins;


import org.apache.commons.io.FileUtils;
import org.apache.commons.io.IOUtils;
import org.springframework.scheduling.concurrent.ThreadPoolTaskExecutor;

import java.io.*;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;

/**
 * @author: lt
 * @date: 2022/6/20 16:04
 * @Description: todo
 */
public class EmptyTest {
    private final static String Separator = "/"; // File.separator;

    /*public static void main(String[] args) {
        *//*new Random(47)
                .ints(5,20)
                .distinct()
                .limit(7)
                .sorted()
                .forEach(System.out::println);
        *//*

        try {
            BufferedReader bufferedReader = new BufferedReader(new FileReader("C:\\Users\\Administrator.DESKTOP-4PCK80Q\\Desktop\\match\\sepPoint.txt"));

            String collect = bufferedReader.lines().collect(Collectors.joining("\n"));
            System.out.println(collect);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
    }*/
    
    
    public static void main1(String[] args) throws IOException, ParseException {

        //List<String> strings = Arrays.asList("1", "2", "3", "4");
        ////strings.stream().collect(ArrayList::new, ArrayList::add, ArrayList::addAll).forEach(System.out::println);
        //
        //String concat = strings.stream().collect(StringBuilder::new, StringBuilder::append,
        //                                               StringBuilder::append)
        //                             .toString();
        //System.out.println(concat);

       /* Path path = Paths.get("C:\\Users\\Administrator.DESKTOP-4PCK80Q\\Desktop\\match\\sepPoint.txt");
        Stream<String> fileStream = Files.lines(path);
        Stream<String> fileStreamWithCharset = Files.lines(path, Charset.forName("UTF-8"));

       fileStream.collect(Collectors.toList()).forEach(System.out::println);

        BaseEntity entity = BaseEntity.builder().name("张三").id(1).build();
        System.out.println(entity);*/

        File file = new File("C:\\Users\\Administrator.DESKTOP-4PCK80Q\\Desktop\\test\\1.txt");
        //FileUtils.forceDelete(file);

        //FileTime fileTime = Files.readAttributes(Paths.get("C:\\Users\\Administrator.DESKTOP-4PCK80Q\\Desktop\\test"), BasicFileAttributes.class).creationTime();
        //SimpleDateFormat sf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        //String format = sf.format(fileTime.toMillis());
        //System.out.println(format);
        //
        //long lastModified = file.lastModified();
        //String format1 = sf.format(lastModified);
        //System.out.println("format1"+format1);


        //File file1 = new File("E:\\test");
        //System.out.println(file.length());

        File sourceDir = new File("C:\\Users\\Administrator.DESKTOP-4PCK80Q\\Desktop\\1\\aa");

        File[] files = sourceDir.listFiles();
        for (File file1 : files) {
            FileUtils.copyFileToDirectory(file1,new File("C:\\Users\\Administrator.DESKTOP-4PCK80Q\\Desktop\\1\\copy"));
        }
    }

    
    public static void test() throws ExecutionException, InterruptedException {
        ThreadPoolTaskExecutor threadPoolTaskExecutor = (ThreadPoolTaskExecutor) Executors.newCachedThreadPool();
        List<Integer> integers = Arrays.asList(1, 2, 3, 4);
        List<Future<Integer>> futureList = new ArrayList<>();
        integers.forEach(i->{
            futureList.add(threadPoolTaskExecutor.submit(()->{
                return i+1;
            }));
        });
        for (Future<Integer> integerFuture : futureList) {
            System.out.println(integerFuture.get());
        }
    }
    
    

    public static void main(String[] args) throws ExecutionException, InterruptedException {
      /*  ExecutorService executorService = Executors.newCachedThreadPool();
        List<Integer> integers = Arrays.asList(1, 2, 3, 4);
        List<Future<Integer>> futureList = new ArrayList<>();
        integers.forEach(i->{
            futureList.add(executorService.submit(()->{
                return i+1;
            }));
        });
        for (Future<Integer> integerFuture : futureList) {
            System.out.println(integerFuture.get());
        }
        executorService.shutdown();*/

        String s = "/data/ring/deploy/static/cornerstone/1001/1.2.156.110002.21.2017.2.6.143949375.1/1.3.12.2.1107.5.1.4.73793.30000017020600090379400043555_FFR/calcificationScore/outAgastonMaskMhd.mhd";
        int i = s.lastIndexOf("/");
        String substring = s.substring(0, i);
        System.out.println(substring);
        String cprPng = String.format("%s%s%s%s%s", substring, Separator, "cprPng", Separator, "1.png");
        System.out.println(cprPng);

        try {
            FileInputStream inputStream = new FileInputStream(new File("C:\\Users\\Administrator.DESKTOP-4PCK80Q\\Desktop\\通过list的方式调用中台脱敏.txt"));
            FileOutputStream outputStream = new FileOutputStream(new File("C:\\Users\\Administrator.DESKTOP-4PCK80Q\\Desktop\\2.txt"));
            IOUtils.copy(inputStream,outputStream);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
