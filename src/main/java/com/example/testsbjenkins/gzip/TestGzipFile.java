package com.example.testsbjenkins.gzip;

import java.io.*;
import java.util.zip.GZIPInputStream;
import java.util.zip.GZIPOutputStream;

/**
 * @author: lt
 * @date: 2022/6/24 9:41
 * @Description: todo
 */
public class TestGzipFile {
    
    
    public static void gzipFile(String url){
        File file = new File(url);
        try {
            InputStream in = new BufferedInputStream(new FileInputStream(file));
            OutputStream out = new BufferedOutputStream(new GZIPOutputStream(new FileOutputStream("C:\\Users\\Administrator.DESKTOP-4PCK80Q\\Desktop\\test\\test.gz")));
            int c;
            while ((c = in.read()) != -1){
                out.write(c);
            }
            in.close();
            out.close();
            System.out.println("gzip数据完成");
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }catch (IOException e) {
            e.printStackTrace();
        }

    }

    public static void unGzFile(String inFileName, String outFileName) {

        try {
            GZIPInputStream in = new GZIPInputStream(new FileInputStream(inFileName));


            FileOutputStream out = new FileOutputStream(outFileName);
            byte[] buffer = new byte[1024];
            int len;
            while ((len = in.read(buffer)) !=-1){
                out.write(buffer,0,len);
            }
            in.close();
            out.close();

        } catch (IOException e) {
            e.printStackTrace();
        }


    }



        public static void main(String[] args) {
        //gzipFile("C:\\Users\\Administrator.DESKTOP-4PCK80Q\\Desktop\\test\\1.txt");

            unGzFile("C:\\Users\\Administrator.DESKTOP-4PCK80Q\\Desktop\\test\\test.gz","C:\\Users\\Administrator.DESKTOP-4PCK80Q\\Desktop\\test\\12.txt");
    }
}
