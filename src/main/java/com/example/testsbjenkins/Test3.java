package com.example.testsbjenkins;

import cn.hutool.json.JSONObject;
import lombok.extern.slf4j.Slf4j;

import java.io.*;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @author: lt
 * @date: 2022/6/14 10:43
 * @Description: todo
 */
@Slf4j
public class Test3 {

    /**
     * text文件读取并转成JSON格式
     */
    public static JSONObject readText(String text){
        File file = new File(text);
        String jsonStr = "";
        FileInputStream fis = null;
        InputStreamReader isr = null;
        BufferedReader br = null;
        try{
            fis = new FileInputStream(file);
            isr = new InputStreamReader(fis,"utf-8");
            br = new BufferedReader(isr);
            String line = "";
            while((line = br.readLine())!= null){
                jsonStr += line;
            }
            return new JSONObject(jsonStr);
        }catch (IOException e){
            e.printStackTrace();
            return null;
        }finally {
            try {
                fis.close();
                isr.close();
                br.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

    }

    
    public static String parseCoroLesionDetectionFile(){
        String url= "C:\\Users\\Administrator.DESKTOP-4PCK80Q\\Desktop\\match\\CoroLesionDetection.txt";
        String results ="";
        try {
            BufferedReader fin=new BufferedReader(new FileReader("C:\\Users\\Administrator.DESKTOP-4PCK80Q\\Desktop\\match\\CoroLesionDetection.txt"));
            String read=null;//声明一个字符串变量存放读出的字符串
            while((read=fin.readLine())!=null)//一直读 （只要不是空行或者读完毕）
            {
                results+=read;
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        return results;
    }

    public static String parsesSepPointFile(){
        String url= "C:\\Users\\Administrator.DESKTOP-4PCK80Q\\Desktop\\match\\sepPoint.txt";
        String results ="";
        Map<String, Object> RCAMap = new HashMap<>();
        Map<String, Object> LADMap = new HashMap<>();
        Map<String, Object> LCXMap = new HashMap<>();
        List<String> RCAList = new ArrayList<>();
        List<String> LADList = new ArrayList<>();
        List<String> LCXList = new ArrayList<>();
        List<String> alllines = new ArrayList<>();
        try {
            BufferedReader fin=new BufferedReader(new FileReader("C:\\Users\\Administrator.DESKTOP-4PCK80Q\\Desktop\\match\\sepPoint.txt"));
            String read = null;//声明一个字符串变量存放读出的字符串
            while((read=fin.readLine())!=null)//一直读 （只要不是空行或者读完毕）
            {
                //results+=read;
                alllines.add(read);
            }
            RCAMap.put("RCA",RCAList);
            LADMap.put("LAD",LADList);
            LCXMap.put("LCX",LCXList);
        } catch (IOException e) {
            e.printStackTrace();
        }
        System.out.println("rca:"+RCAMap.toString());
        System.out.println("LAD:"+LADMap.toString());
        System.out.println("LCX:"+LCXMap.toString());
        return results;
    }

    public static boolean isChar(String s){

        boolean result =false;
        char c[] = s.toCharArray();
        byte b[] = new byte[c.length];

        for (int i = 0; i < c.length; i++) {
            b[i] = (byte) c[i];
            // 对每一个字符进行判断
            if ((b[i] >= 97 && b[i] <= 122) || (b[i] >= 65 && b[i] <= 90)) {
                result =true;
            }else {
                result= false;
            }
        }
        return result;
    }

    public static void main(String[] args) throws IOException {


        List<String> strings = Files.readAllLines(Paths.get("C:\\Users\\Administrator.DESKTOP-4PCK80Q\\Desktop\\match\\sepPoint.txt"));

      /*  List<String> RCAList = new ArrayList<>();
        List<String> LADList = new ArrayList<>();
        List<String> LCXList = new ArrayList<>();
        int size = strings.size();
        for (int i = 0; i < size; i++) {
            if (i+1<size) {
                String temp0 = strings.get(i);
                String temp1 = strings.get(i+1);
                
                if(temp0.contains("RCA")){
                    continue;
                }
                if(!temp1.contains("LAD") && !temp1.contains("LCX")){
                    RCAList.add(temp1);
                }else 
                if(!temp1.contains("RCA") && !temp1.contains("LCX")){
                    LADList.add(temp1);
                }else 
                if(!temp1.contains("RCA") && !temp1.contains("LAD")){
                    LCXList.add(temp1);
                }else {
                }
            }
        }*/

        String allStr = strings.toString();
        String[] lads = allStr.split("LAD:");

        String RCA = lads[0];
        RCA = RCA.replaceFirst("RCA:", "").replaceFirst(",", "").replaceFirst(" ", "").replaceFirst("\\[", "");
        RCA = RCA.substring(0,RCA.length()-2);
        System.out.println("RCA:"+RCA);
        String ladAndLcx = lads[1];
        //System.out.println("ladAndLcx:"+ ladAndLcx);

        String[] lcxs = ladAndLcx.split("LCX:");
        String LAD = lcxs[0];
        LAD= LAD.replaceFirst(",","").replaceFirst(" ","");
        LAD = LAD.substring(0,LAD.length()-2);
        String LCX = lcxs[1];
        LCX = LCX.replaceFirst(",","").replaceFirst(" ","").replaceFirst("]","");
        System.out.println("LAD:"+LAD);
        System.out.println("LCX:"+LCX);

        String[] rcaArray = RCA.split(",");
        String[] ladArray = LAD.split(",");
        String[] lcxArray = LCX.split(",");

        for (String s : rcaArray) {
            System.out.println(s);
        }  for (String s : ladArray) {
            System.out.println(s);
        }  for (String s : lcxArray) {
            System.out.println(s);
        }
        /*for (String lad : lads) {
            if (lad.contains("LCX:")){
                String[] lcxs = lad.split("LCX:");
                //for (String lcx : lcxs) {
                //    lcx.replaceAll("\\]","").replaceFirst(",","").replaceFirst(" ","");
                //    
                //    
                //    //System.out.println(lcx.replaceAll("\\]","").replaceFirst(",","").replaceFirst(" ",""));
                //}
                LADList = Arrays.asList(lcxs[0]);
                LCXList = Arrays.asList(lcxs[1]);
                System.out.println(LADList.toString());
                System.out.println(LCXList.toString());
            }else {
                lad.replaceAll("\\[","").replaceFirst(",","").replaceFirst("RCA:","").replaceFirst(" ","");
                String[] split = lad.split(",");
                for (String s : split) {
                if (!s.isEmpty())RCAList.add(s);
                }
                System.out.println(RCAList.toString());
            }
        }*/
        
        //System.out.println(allStr);
        //System.out.println("RCAList:"+RCAList.toString());
        //System.out.println("LADList:"+LADList.toString());
        //System.out.println("LCXList:"+LCXList.toString());
    
        //parsesSepPointFile();
    }
    
    
   
}
