package com.example.testsbjenkins.mianshi;

/**
 * @author: lt
 * @date: 2022/7/7 16:46
 * @Description: 双重检查锁的线程安全的单例模式
 */
public class SingletonInstance {

    private static volatile SingletonInstance instance = null;

    public SingletonInstance() {
    }

    static SingletonInstance getInstance() {

        if (null == instance) {
            synchronized (SingletonInstance.class){

                if (null == instance) {
                    instance = new SingletonInstance();
                }
            }
            
        }
        return instance;
    }
    
}
