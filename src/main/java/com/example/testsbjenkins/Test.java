package com.example.testsbjenkins;

import cn.hutool.core.thread.NamedThreadFactory;
import lombok.extern.slf4j.Slf4j;

import java.util.concurrent.LinkedBlockingDeque;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;

/**
 * @author: lt
 * @date: 2022/6/14 10:43
 * @Description: todo
 */
@Slf4j
public class Test {
    public static void main(String[] args) throws InterruptedException {


        ThreadPoolExecutor threadPoolExecutor = new ThreadPoolExecutor(2, 4, 60, TimeUnit.SECONDS, new LinkedBlockingDeque<>(2),new NamedThreadFactory("测试",true)) {

            @Override
            protected void beforeExecute(Thread t, Runnable r) {
                log.info("前置钩子被执行");
                super.beforeExecute(t, r);
            }

            @Override
            protected void afterExecute(Runnable r, Throwable t) {
                log.info("后置钩子被执行");
                super.afterExecute(r, t);
            }

            @Override
            protected void terminated() {
                log.info("任务终止");
                super.terminated();
            }
        };
        threadPoolExecutor.execute(()->{
            log.info("任务执行");
        });

        TimeUnit.SECONDS.sleep(10);
        log.info("关闭线程池");
        threadPoolExecutor.shutdown();

    }
}
