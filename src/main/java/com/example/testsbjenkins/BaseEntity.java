package com.example.testsbjenkins;

import lombok.Builder;
import lombok.Data;

/**
 * @author: lt
 * @date: 2022/6/22 17:30
 * @Description: todo
 */
@Data
@Builder(toBuilder = true)
public class BaseEntity {
    private int id;
    private String name;
}
