package com.example.testsbjenkins.CompletableFuture;

import java.util.concurrent.*;

/**
 * @author: lt
 * @date: 2022/8/3 17:03
 * @Description: todo
 */
public class ExecutorCompletionServiceTest {
    public static void main(String[] args) throws InterruptedException, ExecutionException {

        ExecutorService executorService = Executors.newFixedThreadPool(3);
        ExecutorCompletionService service = new ExecutorCompletionService(executorService);
        for (int i = 0 ; i < 5 ;i++) {
            int seqNo = i;
            service.submit(new Callable<String>() {
                @Override
                public String call() throws Exception {
                    return "HelloWorld-" + seqNo + "-" + Thread.currentThread().getName();
                }
            });
        }
        for (int i = 0 ; i < 5 ;i++) {
            System.out.println(service.take().get());
        }

        executorService.shutdown();
    }
}
