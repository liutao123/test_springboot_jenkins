package com.example.testsbjenkins.CompletableFuture;

import lombok.AllArgsConstructor;
import lombok.Data;

/**
 * @author: lt
 * @date: 2022/6/24 16:37
 * @Description: todo
 */
@Data
@AllArgsConstructor
public class UserInfo {
    private String desc;
    private String name;
    private int age;
}
