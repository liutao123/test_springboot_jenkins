package com.example.testsbjenkins.lombokBuilder;

import lombok.Builder;

/**
 * @author: lt
 * @date: 2022/6/23 14:59
 * @Description: 测试lombok @Builder 注解作用于方法
 */
public class TestPerson {
    
    
    @Builder(builderMethodName = "methodBuilder",buildMethodName = "getPerson")
    public static String getPerson(){
        System.out.println("张三");
        return "张三";
    }

    public static void main(String[] args) {

        methodBuilder().getPerson();
    }
}
