package com.example.testsbjenkins.FileTest;

import org.apache.commons.io.FileUtils;

import java.io.File;
import java.math.BigInteger;

/**
 * @author: lt
 * @date: 2022/6/24 14:56
 * @Description: todo
 */
public class TestDirSize {
    public static void main(String[] args) {

        long l = FileUtils.sizeOfDirectory(new File("C:\\Users\\Administrator.DESKTOP-4PCK80Q\\Desktop\\test"));
        BigInteger bigInteger = FileUtils.sizeOfDirectoryAsBigInteger(new File("C:\\Users\\Administrator.DESKTOP-4PCK80Q\\Desktop\\test"));
        System.out.println(l);
        System.out.println(bigInteger);

    }
}
