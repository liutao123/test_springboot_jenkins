package com.example.testsbjenkins.FileTest;

import org.apache.commons.io.FileUtils;
import org.apache.commons.io.filefilter.*;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.nio.file.attribute.BasicFileAttributes;
import java.nio.file.attribute.FileTime;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDateTime;
import java.time.ZoneOffset;
import java.util.Calendar;
import java.util.Date;
import java.util.Iterator;


/**
 * @author: lt
 * @date: 2022/7/6 10:07
 * @Description: todo
 */
public class TestCleanFiles {
    
    private static  final   SimpleDateFormat sf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
    
    
    static boolean checkDirectoryExpired(long fileTimeMillis,  long expire){
        boolean result = false;
        long now = System.currentTimeMillis();
        if (now - fileTimeMillis>expire){
            result =  true;
        }
        return result;
    }
    
    
    static boolean checkDirectoryExpired2(String dirPath,  long expire){

        boolean result = false;
        long now = System.currentTimeMillis();

        IOFileFilter timeFileFilter = FileFilterUtils.ageFileFilter(new Date(now - expire), true);
        IOFileFilter fileFiles = new AndFileFilter(FileFileFilter.FILE, timeFileFilter);
        File directory = new File(dirPath);
        Iterator<File> itFile = FileUtils.iterateFiles(directory, fileFiles, TrueFileFilter.INSTANCE);
        // 删除符合条件的文件
        while (itFile.hasNext()) {
            result =  true;
            break;
            //delete(itFile.next());
        }
        return result;
    }

    /**
     * 
     * @param fileTimeMillis 文件创建时间
     * @param mount  多少天后过期
     * @return
     */
    static boolean checkDirectoryExpired3(long fileTimeMillis,  int  mount){

        boolean result = false;

        Calendar calendar = Calendar.getInstance();
        calendar.setTimeInMillis(fileTimeMillis);
        System.out.println("创建时间: "+sf.format(calendar.getTimeInMillis()));
        calendar.add(Calendar.DAY_OF_MONTH,mount);

        System.out.println("过期时间: "+sf.format(calendar.getTimeInMillis()));
        if(System.currentTimeMillis() > calendar.getTimeInMillis()){
            result = true;
        }
        return  result;

    }


    /**
     * 通过localdate来处理
     * @param fileCreateTimeMillis
     * @param mount
     * @return
     */
    static boolean checkDirectoryExpired4(long fileCreateTimeMillis,  int  mount){

        boolean result = false;

        LocalDateTime now = LocalDateTime.now();

        LocalDateTime createTime = new Date(fileCreateTimeMillis).toInstant().atOffset(ZoneOffset.of("+8")).toLocalDateTime();
        LocalDateTime localDateTime = createTime.plusDays(mount);

      
        System.out.println("创建时间: "+createTime.toString());

        System.out.println("过期时间: "+localDateTime.toString());
        if(now.isAfter(localDateTime)){
            result = true;
        }
        return  result;

    }
    
    
    public static void main(String[] args) throws IOException, ParseException {
        File file = new File("C:\\Users\\Administrator.DESKTOP-4PCK80Q\\Desktop\\test");
        System.out.println(file.length());

        System.out.println(FileUtils.sizeOfDirectoryAsBigInteger(file));

        //String path = "C:\\Users\\Administrator.DESKTOP-4PCK80Q\\Desktop\\test";
        FileTime fileTime = Files.readAttributes(Paths.get(file.getAbsolutePath()), BasicFileAttributes.class).creationTime();
      
        long fileTimeMillis = fileTime.toMillis();
        String format = sf.format(fileTimeMillis);
        System.out.println(format);

        //long lastModified = file.lastModified();
        //String format1 = sf.format(lastModified);
        //System.out.println("format1"+format1);
        
        //check File is expired
      
        //expire time 7days
        long expire = 1000 * 60 *60  *24;

        long time = sf.parse("2022-07-05 19:40:35").getTime();

        Calendar calendar = Calendar.getInstance();
        calendar.setTime(sf.parse("2022-07-05 13:00:35"));
        calendar.add(Calendar.DAY_OF_MONTH,1);

        //System.out.println(calendar.getTime());
        
        
        if(checkDirectoryExpired4(time,3)){
            System.out.println("过期");
        }else{
            System.out.println("未过期");
        }
    }
}
