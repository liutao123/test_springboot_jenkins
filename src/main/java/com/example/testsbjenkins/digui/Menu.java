package com.example.testsbjenkins.digui;

import lombok.Builder;
import lombok.Data;

import java.util.List;

/**
 * @author: lt
 * @date: 2022/6/24 14:10
 * @Description: todo
 */
@Data
@Builder
public class Menu {

    public Integer id;

    public String name;

    public Integer parentId;

    public List<Menu> childList;


    public Menu(Integer id, String name, Integer parentId) {
        this.id = id;
        this.name = name;
        this.parentId = parentId;
    }

    public Menu(Integer id, String name, Integer parentId,List<Menu> childList) {
        this.id = id;
        this.name = name;
        this.parentId = parentId;
        this.childList = childList;
    }

}