package com.example.testsbjenkins.DynamicProxyAndReflex;

import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.util.Arrays;

/**
 * @author: lt
 * @date: 2022/7/7 10:59
 * @Description: todo
 */
public class Person1 {
    private int age;
    private String name;
    public Person1() {
    }
    private Person1(String name){
        this.name = name;
    }
    public Person1(int age, String name){
        this.age = age;
        this.name = name;
    }
    //省略set/get方法
    @Override
    public String toString() {
        return "Person1{" +
                "age=" + age +
                ", name='" + name + '\'' +
                '}';
    }

    public static void main(String[] args) throws ClassNotFoundException, NoSuchMethodException, IllegalAccessException, InvocationTargetException, InstantiationException {
        //此处调用的是内部类 所以需要与外部类 通过 $ 拼接
        Class<?> PersonClass = Class.forName("com.example.testsbjenkins.DynamicProxyAndReflex.Person1");

        //获取所有类型的构造函数
        Constructor<?>[] declaredConstructors = PersonClass.getDeclaredConstructors();
        System.out.println("所有的构造函数如下:");
        Arrays.stream(declaredConstructors).forEach(System.out::println);

        //获取所有PUBLIC类型的构造函数
        Constructor<?>[] constructors = PersonClass.getConstructors();
        System.out.println("所有PUBLIC类型的构造函数如下:");
        Arrays.stream(constructors).forEach(System.out::println);

        //通过反射调用 类的构造函数 私有 | 公有
        Constructor<?> privateConstructor = PersonClass.getDeclaredConstructor(String.class);
        //Person类的带一个String 参数的构造函数是私有的 需要取消 Java 语言的访问检查
        privateConstructor.setAccessible(true);
        Object instance = privateConstructor.newInstance("张三");
        System.out.println("Person类的带一个String 参数的构造函数,返回的对象信息是:"+instance.toString());
    }
}
