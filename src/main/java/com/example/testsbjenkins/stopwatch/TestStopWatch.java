package com.example.testsbjenkins.stopwatch;


import org.springframework.util.StopWatch;

import java.util.concurrent.TimeUnit;

/**
 * @author: lt
 * @date: 2022/7/1 11:11
 * @Description: todo
 */
public class TestStopWatch {

    public static void main(String[] args) throws InterruptedException {

        StopWatch task1 = new StopWatch();
        task1.start();
        TimeUnit.SECONDS.sleep(3);
        task1.stop();
        double totalTimeSeconds = task1.getTotalTimeSeconds();
        String s = task1.prettyPrint();
        System.out.println("s:"+s);
        System.out.println("LastTaskTimeMillis:"+task1.getLastTaskTimeMillis());
        System.out.println("totalTimeSeconds:"+totalTimeSeconds);
    }
}
