package com.example.testsbjenkins;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class TestsbjenkinsApplication {

    public static void main(String[] args) {
        SpringApplication.run(TestsbjenkinsApplication.class, args);
    }

}
