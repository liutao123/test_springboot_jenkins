package com.example.testsbjenkins;

import cn.hutool.core.io.FileUtil;
import cn.hutool.json.JSONObject;
import lombok.extern.slf4j.Slf4j;

import java.io.*;

/**
 * @author: lt
 * @date: 2022/6/14 10:43
 * @Description: todo
 */
@Slf4j
public class Test2 {

    /**
     * text文件读取并转成JSON格式
     */
    public static JSONObject readText(String text){
        File file = new File(text);
        String jsonStr = "";
        FileInputStream fis = null;
        InputStreamReader isr = null;
        BufferedReader br = null;
        try{
            fis = new FileInputStream(file);
            isr = new InputStreamReader(fis,"utf-8");
            br = new BufferedReader(isr);
            String line = "";
            while((line = br.readLine())!= null){
                jsonStr += line;
            }
            return new JSONObject(jsonStr);
        }catch (IOException e){
            e.printStackTrace();
            return null;
        }finally {
            try {
                fis.close();
                isr.close();
                br.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

    }


    public static void main(String[] args)  {

      /*  JSONObject jsonObject = readText("C:\\Users\\Administrator.DESKTOP-4PCK80Q\\Desktop\\match\\CoroLesionDetection.txt");

        System.out.println(jsonObject.toString());*/
        //System.out.println(UUID.randomUUID().toString().replaceAll("-",""));
        //String a ="/data/ring/deploy/static/cornerstone/1001/1.2.826.0.1.3680043.6.30985.21382.20130430072953.888.34/1.3.12.2.1107.5.1.4.73649.30000013042923550398400015331/000011.dcm";
        //int i = a.lastIndexOf("/");
        //String substring = a.substring(0, i);
        //System.out.println(substring);

        File file = new File("C:\\Users\\Administrator.DESKTOP-4PCK80Q\\Desktop\\2");
        long size = FileUtil.size(file);
        System.out.println("文件夹长度:"+size);
    }
}
