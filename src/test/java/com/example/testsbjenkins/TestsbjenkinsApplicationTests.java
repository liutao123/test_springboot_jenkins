package com.example.testsbjenkins;

import com.example.testsbjenkins.StringRedisTemplate.RedisUtils;
import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.Arrays;
import java.util.List;

@SpringBootTest
@RunWith(SpringRunner.class)
class TestsbjenkinsApplicationTests {

    @Autowired
    private RedisUtils redisUtils;

    @Test
    void contextLoads() {

        List<Integer> integers = Arrays.asList(0, 1, 2, 3, 4);
        Integer integer = integers.stream().filter(i -> integers.contains(0)).findFirst().get();

        if (integer!= null){
            System.out.println("选择了0");
        }
        boolean zeroMatch = integers.stream().anyMatch(i -> integers.contains(0));
        boolean oneMatch = integers.stream().anyMatch(i -> integers.contains(1));
        System.out.println("zeroMatch:"+zeroMatch+" oneMatch:"+oneMatch);
        

    }


    //操作字符串
    @Test
    public void opsForValue() {
        redisUtils.set("蜀国创始人", "刘备1"); //设置值
        String name = redisUtils.get("蜀国创始人");//取出值
        System.out.println(name);
    }

    
    
    @Test
    void testRedis(){
        
    }

}
