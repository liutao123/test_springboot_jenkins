FROM java:8
VOLUME /tmp
ADD target/*.jar myTestSp.jar
EXPOSE 8080
ENTRYPOINT ["java","-jar","myTestSp.jar","&"]